package br.com.proway.Olimpiadas.controllers;

import br.com.proway.Olimpiadas.models.Bicicleta;
import br.com.proway.Olimpiadas.models.Ciclismo;
import br.com.proway.Olimpiadas.repositories.BicicletaRepository;
import br.com.proway.Olimpiadas.repositories.CiclismoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/bicicleta")
public class BicicletaController {

    @Autowired
    private BicicletaRepository bicicletaRepository;

    @Autowired
    private CiclismoRepository ciclismoRepository;

    @PostMapping("/modalidade/{idModalidade}")
    public ResponseEntity addBicicleta(
            @RequestBody Bicicleta body,
            @PathVariable("idModalidade") int idModalidade
    ) {
        try {
            Ciclismo ciclismo = ciclismoRepository.findById(idModalidade);

            body.setCiclismo(ciclismo);
            body = bicicletaRepository.save(body);
            return new ResponseEntity<>(body, HttpStatus.CREATED);
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());

            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<ArrayList<Bicicleta>> getAllBicicletas() {
        try {
            return new ResponseEntity<>(bicicletaRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());

            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
