package br.com.proway.Olimpiadas.controllers;

import br.com.proway.Olimpiadas.models.Ciclismo;
import br.com.proway.Olimpiadas.repositories.CiclismoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/ciclismo")
public class CiclismoController {

    @Autowired
    private CiclismoRepository ciclismoRepository;

    @PostMapping
    public ResponseEntity<Object> addCiclismo(@RequestBody Ciclismo body) {
        try {
            body = ciclismoRepository.save(body);

            return new ResponseEntity<>(body, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Falha ao cadastrar modalidade", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<ArrayList<Ciclismo>> getAllCiclismo() {
        try {
            return new ResponseEntity<>(
                    ciclismoRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());

            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }
}
