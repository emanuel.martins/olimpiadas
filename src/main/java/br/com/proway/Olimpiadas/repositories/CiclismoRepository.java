package br.com.proway.Olimpiadas.repositories;

import br.com.proway.Olimpiadas.models.Ciclismo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface CiclismoRepository extends JpaRepository<Ciclismo, Long> {
    Ciclismo save(Ciclismo ciclismo);
    Ciclismo findById(long id);
    ArrayList<Ciclismo> findAll();
}
