package br.com.proway.Olimpiadas.repositories;

import br.com.proway.Olimpiadas.models.Bicicleta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface BicicletaRepository extends JpaRepository<Bicicleta, Long> {
    Bicicleta save(Bicicleta bicicleta);
    ArrayList<Bicicleta> findAll();
}
