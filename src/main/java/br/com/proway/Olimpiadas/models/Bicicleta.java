package br.com.proway.Olimpiadas.models;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "bicicletas")
public class Bicicleta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column private String customizacao;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_id_ciclismo")
    private Ciclismo ciclismo;
}
